# Deployment of techy Services

Follwoing figure discussed the architecture of the techy platfrom. 

![Alt text](techy-architecture.png?raw=true "techy platfrom architecture")

## Services

There are three main servies;

```
1. elassandra
2. aplos
3. gateway
```



## Deploy services

Start services in following order;

```
docker-compose up -d elassandra
docker-compose up -d aplos
docker-compose up -d gateway
docker-compose up -d web
```
** After hosting website, it can be reached on `<Ip address of the Vm>:4300`.

** Open `7654` and `4300` port on VM for public

#### 1. put Employee

```
# request
curl -XPOST "http://localhost:7654/api/employee" \
--header "Content-Type: application/json" \
--header "Bearer: eyJkaWdzaWciOiJORWpYZnIwQjJMZG4ySGxPb2t5blp0dkNzSFVqMGFoVTVZd1F5TmJSVCtOYjlwTnBXcEsvUi9UbDZpanhPVVJiVlJHc2NHaFIrcWVCbkZhK09YYjBmMGlacVh0WHBDVXV6bnJOcTFKRmpGZC8zSU80L1o4SXl3WG1EdWFGcUg5Njc5VE9neVRkcU1nT01VeWNNWTF0bmtIUStWVUtUN0JTV0NWMEM3ZmNXbEE9IiwiaWQiOiJlcmFuZ2FlYkBnbWFpbC5jb20iLCJpc3N1ZVRpbWUiOjE1NTg0ODk4ODksInJvbGVzIjoiIiwidHRsIjo2MH0=" \
--data '
{
  "id": "emp025",
  "fullname": "charith Vandebona",
  "mobileNo": "0715144989",
  "email": "madushancw@gmail.com"
  
}
'

# reply
{"code":201,"msg":"employee details added"}
```


#### 2. get Employee 

```
# request
curl -XPOST "http://localhost:7654/api/employee" \
--header "Content-Type: application/json" \
--header "Bearer: eyJkaWdzaWciOiJORWpYZnIwQjJMZG4ySGxPb2t5blp0dkNzSFVqMGFoVTVZd1F5TmJSVCtOYjlwTnBXcEsvUi9UbDZpanhPVVJiVlJHc2NHaFIrcWVCbkZhK09YYjBmMGlacVh0WHBDVXV6bnJOcTFKRmpGZC8zSU80L1o4SXl3WG1EdWFGcUg5Njc5VE9neVRkcU1nT01VeWNNWTF0bmtIUStWVUtUN0JTV0NWMEM3ZmNXbEE9IiwiaWQiOiJlcmFuZ2FlYkBnbWFpbC5jb20iLCJpc3N1ZVRpbWUiOjE1NTg0ODk4ODksInJvbGVzIjoiIiwidHRsIjo2MH0=" \
--data '
{
  "id": "emp025",
  "fullname": "charith Vandebona",
  "mobileNo": "0715144989",
  "email": "madushancw@gmail.com",
}
'

# reply
{"fullname":"charith Vandebona","employeeStatus": "done", "timestamp":"2021-11-05 20:55:21.308"}
```
